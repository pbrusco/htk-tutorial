#!/bin/bash
n=0

function clean {
	rm -rf $1
	mkdir $1
	echo Cleaning directory: $1
}

function pause {
	read -p "$1" str
	case $str in
    	[yY]| [sS]|[yY] | [Yy]es) 
		exit
		;;
	    *)
	esac
}

function re-estimate {
	nplus1=$(($n + 1))
	mkdir Models/hmm$nplus1
	HERest -T 1 -C Configs/HERest.config -I Data/train-phones.mlf -t 250.0 150.0 10000.0 -S Scripts/HERest.script -H Models/hmm$n/macros -H Models/hmm$n/hmmdefs -M Models/hmm$nplus1 Dictionary/phones.list
	increment
}

function increment_mixtures {
	nplus1=$(($n + 1))
	mkdir Models/hmm$nplus1
	HHEd -H Models/hmm$n/macros -H Models/hmm$n/hmmdefs -M Models/hmm$nplus1 Configs/HHEdIncrementMixtures.conf Dictionary/phones.list
	increment
}

function test_model {
	echo "Testing data with HVite (model $n)"
	HVite -H Models/hmm$n/macros -H Models/hmm$n/hmmdefs -S Scripts/HVite.script -i $1 -w Dictionary/DictionarySources/grammar.wordnet -p 0.0 -s 5.0 Dictionary/phones.dict Dictionary/phones.list
	echo "Done"
}

function increment {
	nplus1=$(($n + 1))
	echo "Incrementing from $n -> $nplus1"
	n=$nplus1
}

clean Data/Train/
clean Data/Tests/
clean Scripts/

rm Data/train-words.mlf
rm Data/train-phones.mlf

for hmm in `find ./Models/* -type d`
do
  rm -rf $hmm
done

mkdir mkdir Models/hmm0

echo "Dictionary and phones lists"
HDMan -m -w Dictionary/DictionarySources/words-sorted.list -n Dictionary/phones.list -l dlog Dictionary/phones.dict Dictionary/DictionarySources/dict
echo "SENT-END	[] sil" >> Dictionary/phones.dict # [] it will not be shown in the results.
echo "SENT-START	[] sil" >> Dictionary/phones.dict # asume 50ms al principio y al final de cada grabacion
echo "SIL		[] sil" >> Dictionary/phones.dict
sort Dictionary/phones.dict >> temp
mv temp Dictionary/phones.dict
echo sil >> Dictionary/phones.list
echo "Done"

echo "Master label files (.mlf)"
Helpers/generate_word_mlf_from_training.rb
HLEd -d Dictionary/phones.dict -i Data/train-phones.mlf Configs/HLEd.config Data/train-words.mlf
echo "Done"

echo "Creating MFCCs (HCopy commands)"
Helpers/generate_hcopy_script.rb
HCopy -T 1 -C Configs/HCopy.config -S Scripts/HCopy.script
echo "Done"

#ONLY ON LINUX
echo "Creating grammar network"
if [[ $(uname) == "Linux" ]];then
	HParse Dictionary/DictionarySources/grammar Dictionary/DictionarySources/grammar.wordnet 
	echo "Ready"
else
	echo "Avoiding this step, HParse works only on linux"
fi

echo "Generating tests MFCCs and HVite script"
Helpers/generate_hcopy_tests_script.rb 
HCopy -T 0 -C Configs/HCopyTests.config -S Scripts/HCopyTests.script
Helpers/generate_hvite_script.rb 
echo "Done"

echo "Training first prototype model"
Helpers/generate_hcompv_script.rb
HCompV -C Configs/HCompV.config -f 0.01 -m -S Scripts/HCompV.script -M Models/hmm0 Models/prototype
echo "Done"

echo "Creating HMM0/HMMDefs and HMM0/macros"
Helpers/create_hmm_defs.sh
Helpers/create_macros.sh
echo "Done"

echo "Create HERest scripts"
Helpers/generate_herest_script.rb
echo "Done"

echo "First Reestimations"
re-estimate
re-estimate
re-estimate
echo "Done"

echo "Incrementing number of mixtures"
increment_mixtures
re-estimate
re-estimate
re-estimate
test_model "recout.mlf" #for the tutorial. 
re-estimate
re-estimate
echo "Done"

echo "Incrementing number of mixtures"
increment_mixtures
re-estimate
re-estimate
re-estimate
re-estimate
echo "Done"

echo "Incrementing number of mixtures"
increment_mixtures
re-estimate
re-estimate
re-estimate
re-estimate
re-estimate
echo "Done"

echo "Incrementing number of mixtures"
increment_mixtures
re-estimate
re-estimate
re-estimate
re-estimate
re-estimate
echo "Done"

test_model "output.mlf"

cat output.mlf


#HVite -H Models/hmm$n/macros -H Models/hmm$n/hmmdefs -C Configs/Live.config -w Dictionary/DictionarySources/grammar.wordnet -p 0.0 -s 5.0 Dictionary/phones.dict Dictionary/phones.list





