#!/usr/bin/env ruby
#normalize label files. 
target = ARGV[0]
Dir["#{target}/**/*.lab"].each do |lab|
  text = File.read lab
  normalized_text = text.gsub(/(\d.\d*)/) {|n| "%09d" % (n.to_f*10000000).to_i}
  File.open(lab, "w") {|file| file.puts normalized_text}
end