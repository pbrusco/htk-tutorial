#!/usr/bin/env ruby
# fills HCopy.script with lines with tests mfcs
tests_utterances_folder = "./Data/Recorded/waves/tests/*"
mfc_tests_folder = "./Data/Tests/"

hcopy_script_path = "./Scripts/HCopyTests.script"

File.open(hcopy_script_path, "w") do |io|  
  Dir[tests_utterances_folder].each do |tests_folder|
    tests_folder_name = File.basename(tests_folder)
    Dir.glob("#{tests_folder}/*.wav").each do |wav|
      wav_name = File.basename(wav).sub(".wav","")
      io.write("#{wav}  #{mfc_tests_folder}#{tests_folder_name}-#{wav_name}.mfc \n")
    end
  end
end
