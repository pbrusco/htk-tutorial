#!/usr/bin/env ruby

silhmm = false
in_state = false
state3 = ""
File.open(ARGV[0]).each do |line|
	in_state = false if line == "<STATE> 4\n"
	(state3 << line) if silhmm && in_state
	silhmm = true if line == "~h \"sil\"\n"
	in_state = true if silhmm && line == "<STATE> 3\n" 
end

File.open(ARGV[0], 'a') do |file|
	file.puts File.read("Models/sp_hmm").gsub("SIL_STATE_3",state3)
end
#replace in Models/sp_hmm and copy to ARGV[0]