#!/usr/bin/env ruby
keyword = ARGV[0]
keyword_replacement = ARGV[1] || keyword.downcase
last_time = ""
directory = "./Data/Recorded/waves/train/"
Dir.glob("#{directory}/*/*.wrd").each do |wrd|
	name = File.basename(wrd).sub(".wrd","")
	folder = File.dirname(wrd)
	File.open("#{folder}/#{name}.lab",'w') do |out|
		File.open(wrd).each_with_index do |line,i|
			if i >= 8
				splitted_line = line.split
				time = (splitted_line[0].sub(".","") << "0")
				label = splitted_line[2].downcase == keyword.downcase ? keyword_replacement : "fill" 
				if i == 8
					out << "00000000  #{time.to_i/2}  sil\n"
					out << "#{time.to_i/2} #{time}    #{label} \n"
				else
					out << "#{last_time}  #{time}  #{label} \n"
				end
				last_time = time
			end
		end
	end
end

#1 second = 10000000 (100nanoseconds)
#example: 
#00000000	04803630	fill
#48036300	31646000	sil
