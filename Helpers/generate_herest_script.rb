#!/usr/bin/env ruby
# fills HRest.script with lines with the following sintax:
#./Data/Train/train1-7.mfc

herest_scripts_path = "./Scripts/HERest.script"
mfc_trainings_folder = "./Data/Train"

File.open(herest_scripts_path, "w") { |io|  
    Dir.glob("#{mfc_trainings_folder}/*.mfc").each do |mfc|
      io.write("#{mfc}\n")
    end
}
