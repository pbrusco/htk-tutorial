#!/usr/bin/env ruby
# fills HVite.script with lines with the following sintax:
#./Data/Test/test-7.mfc

hvite_script_folder = "./Scripts/HVite.script"
mfc_tests_folder = "./Data/Tests"

File.open(hvite_script_folder, "w") { |io|  
    Dir["#{mfc_tests_folder}/*.mfc"].each do |mfc|
      io.write("#{mfc}\n")
    end
}
