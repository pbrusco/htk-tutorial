#!/usr/bin/env ruby

# fills HCopy.script with lines with trainings mfcs
training_utterances_folder = "./Data/Recorded/waves/train/*"
mfc_trainings_folder = "./Data/Train/"

hcopy_script_path = "./Scripts/HCopy.script"
File.open(hcopy_script_path, "w") do |io|  
  Dir[training_utterances_folder].each do |train_folder|
    train_folder_name = File.basename(train_folder)
    Dir.glob("#{train_folder}/*.wav").each do |wav|
      wav_name = File.basename(wav).sub(".wav","")
      io.write("#{wav}  #{mfc_trainings_folder}#{train_folder_name}-#{wav_name}.mfc \n")
    end
  end
end
