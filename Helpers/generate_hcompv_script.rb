#!/usr/bin/env ruby

# fills HCompV.script with lines with the following sintax:
#./Data/Train/train1-7.mfc

hcompv_scripts_path = "./Scripts/HCompV.script"
mfc_trainings_folder = "./Data/Train"

File.open(hcompv_scripts_path, "w") { |io|  
    Dir["#{mfc_trainings_folder}/*.mfc"].each do |mfc|
      io.write("#{mfc}\n")
    end
}
