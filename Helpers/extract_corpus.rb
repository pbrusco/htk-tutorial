#!/usr/bin/env ruby

#Extracts wavs and wrds from the "dir" folder. 
require 'FileUtils'
dir = ARGV[0]
folder = ARGV[1]
system("mkdir ./Data/Recorded/waves/train/#{folder}")

Dir.glob("#{dir}/**/*.wav").each do |wav|
	name = File.basename(wav)
	system "sox #{wav} ./Data/Recorded/waves/train/#{folder}/#{name}"
end 

Dir.glob("#{dir}/**/*.wrd").each do |wrd|
	name = File.basename(wrd)
	FileUtils.cp(wrd,"./Data/Recorded/waves/train/#{folder}/#{name}")
end  

