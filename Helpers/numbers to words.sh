for i in ./* ; do
  mv "$i/1.wav" "$i/uno.wav"
  mv "$i/2.wav" "$i/dos.wav"
  mv "$i/3.wav" "$i/tres.wav"
  mv "$i/4.wav" "$i/cuatro.wav"
  mv "$i/5.wav" "$i/cinco.wav"
  mv "$i/6.wav" "$i/seis.wav"
  mv "$i/7.wav" "$i/siete.wav"
  mv "$i/8.wav" "$i/ocho.wav"
  mv "$i/9.wav" "$i/nueve.wav"
  mv "$i/0.wav" "$i/cero.wav"
done