#!/usr/bin/env ruby
training_utterances_folder = "./Data/Recorded/waves/train/*"
mfc_trainings_folder = "./Data/Train/"

mfccs = "./Data/train-words.mlf"

File.open(mfccs, "w") do |io|
  io.write("#!MLF!#\n")  
  
  Dir.glob(training_utterances_folder).each do |train_folder|
    train_folder_name = File.basename(train_folder)
    Dir.glob("#{train_folder}/*.wav").each do |wav|
      wav_name = File.basename(wav).sub(".wav","")
      lab = wav.sub(".wav",".lab")
      if File.exists? lab
	      label_text = File.read(lab).gsub(/(\d*\.\d*)/) {|n| "%09d" % (n.to_f*10000000).to_i}
	      io.write("\"#{mfc_trainings_folder}#{train_folder_name}-#{wav_name}.lab\"\n#{label_text}\n.\n")
      end
    end
  end

  ["cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"].each do |n|
    io.write("\"*/*-#{n}.lab\"\n#{ARGV[0] || n.upcase}\n.\n")
  end
end